class BlogsController < ApplicationController
  def index
  end

  def new
    @blog = Blog.new
  end

  def create
    @blog = Blog.new(blog_params)
    if @blog.save
      flash[:notice] = 'Blog created succesfully'
      redirect_to blogs_path
    else
      flash[:alert] = @blog.errors
      redirect_to new_blog_path(@blog)
    end
  end

  private

  def blog_params
    params.require(:blog).permit(:title, :contents, :published)
  end
end
