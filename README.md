# README

This website is going to be my own personal website that I will use to blog
about certain technologies that I am using. Notable technologies that I am using
are listed below

* PostgreSQL as the database
* Devise for user authentication
* Byebug for debugging

Future changes:
* Will probably remove turbolinks, as it can complicated JavaScript
* Add bootstrap CSS
* Post to FaceBook and Twitter over API
