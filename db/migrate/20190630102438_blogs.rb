class Blogs < ActiveRecord::Migration[5.2]
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :contents
      t.boolean :published
    end
  end
end
